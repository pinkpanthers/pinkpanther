package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminAddPackageServlet
 */
@WebServlet("/AdminAddPackageServlet")
public class AdminAddPackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminAddPackageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String packageList = Representative.masterPackage.listPackages();
		String customerList = LoginServlet.activeCustomer.listPackages();
		request.setAttribute("packageList", packageList);
		request.setAttribute("customerList", customerList);
		request.getRequestDispatcher("adminAddPackage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("adminAddPackage") != null){
			String packageList = Representative.masterPackage.listPackages();
			String customerList = LoginServlet.activeCustomer.listPackages();
			request.setAttribute("packageList", packageList);
			request.setAttribute("customerList", customerList);
			request.setAttribute("customer", LoginServlet.activeCustomer);
			
			if(request.getParameter("packageOptions") != null){
				String packageName = request.getParameter("packageOptions");
				Package checkPackage;
			
				if(!Representative.allPackages.isEmpty()){
					for(Iterator<Package> iter = Representative.allPackages.listIterator(); iter.hasNext();){
						checkPackage = iter.next();
						
						if(checkPackage.getName().equals(packageName)){
							if(LoginServlet.activeCustomer.addPackage(checkPackage)) {
								System.out.println("ADDING Package: " + packageName);
								request.getRequestDispatcher("AccountOverviewRep.jsp").forward(request, response);
							}
							else {
								System.out.println(packageName + " ALREADY EXISTS");
								RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminAddPackage.jsp");
								PrintWriter out = response.getWriter();
								out.println("<font color=red>Package already exists.</font>");
								rd.include(request, response);
							}
						}
					}
				}
				else {
					System.out.println(packageName + " DOESN'T EXIST");
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminAddPackage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Service doesn't exist.</font>");
					rd.include(request, response);
				}
			}
			else {
				System.out.println("NO CHOICE SELECTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminAddPackage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please select a service from the list.</font>");
				rd.include(request, response);
			}
		}
	}

}
