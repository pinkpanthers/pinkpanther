package com.journaldev.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TurboServlet
 */
@WebServlet("/TurboServlet")
public class TurboServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TurboServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(!LoginServlet.activeCustomer.contains(LoginServlet.Turbo)){
			LoginServlet.activeCustomer.addService(LoginServlet.Turbo);
			//request.setAttribute("customer", LoginServlet.activeCustomer);
			//request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
		}
			request.setAttribute("customer", LoginServlet.activeCustomer);
			request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
		
	}
}
