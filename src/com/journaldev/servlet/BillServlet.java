package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.*;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BillServlet
 */
@WebServlet("/BillServlet")
public class BillServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Customer activeCustomer;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BillServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		double bill = 0;
		double bill1 = LoginServlet.activeCustomer.getBill();
		Service service;
		String serviceList = "";
		String packageList = "No packages";
		double threshold = LoginServlet.activeCustomer.getThreshold();
		DecimalFormat df = new DecimalFormat("#.00");
		
		for(Iterator<Service> iter = LoginServlet.activeCustomer.basket.listIterator(); iter.hasNext();){
			service = iter.next();
			if(LoginServlet.activeCustomer.contains(service)){
				bill += service.getCost();
				serviceList += "Service: " + service.getName() + ", Cost: $" + df.format(service.getCost()) + " Duration: " + service.getDuration() + " months";
				serviceList += '\n';
			}
		}
		
		if (serviceList.length() == 0) {
			serviceList = "No services";
		}
		
		if (LoginServlet.activeCustomer.myPackage != null) {
			bill += LoginServlet.activeCustomer.myPackage.getCost();
			packageList = "Package: " + LoginServlet.activeCustomer.listPackages() + ", Cost: $" + 
					df.format(LoginServlet.activeCustomer.myPackage.getCost()) + " Duration: " + LoginServlet.activeCustomer.myPackage.getDuration() + " months";
		}
		
		request.setAttribute("totalCost", df.format(bill1));
		request.setAttribute("subscribedServices", serviceList);
		request.setAttribute("subscribedPackages", packageList);
		request.setAttribute("customer", activeCustomer);
		if(threshold == Double.MAX_VALUE){
			request.setAttribute("placeholder", "");
			request.setAttribute("threshold", "No Threshold Set");
		}
		else{
			request.setAttribute("placeholder", "$");
			request.setAttribute("threshold", df.format(threshold));
		}
		request.getRequestDispatcher("CustomerViewBill.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String checkThreshold = request.getParameter("Threshold");
		double threshold = LoginServlet.activeCustomer.getThreshold();
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/CustomerViewBill.jsp");
		PrintWriter out = response.getWriter();
		if(checkThreshold.matches("-?\\d+(\\.\\d+)?") ){
			threshold = Double.parseDouble(request.getParameter("Threshold"));
			out.println("<font color=red>Threshold has been set.</font>");
		}
		else{
			out.println("<font color=red>Not a valid amount.</font>");
			//rd.include(request, response);
		}
			
		LoginServlet.activeCustomer.setThreshold(threshold);
		
		double bill = 0;
		double bill1 = LoginServlet.activeCustomer.getBill();
		Service service;
		String serviceList = "";
		String packageList = "No packages";
		DecimalFormat df = new DecimalFormat("#.00");
		
		for(Iterator<Service> iter = LoginServlet.activeCustomer.basket.listIterator(); iter.hasNext();){
			service = iter.next();
			if(LoginServlet.activeCustomer.contains(service)){
				bill += service.getCost();
				serviceList += "Service: " + service.getName() + ", Cost: $" + df.format(service.getCost()) +
						" Duration: " + service.getDuration() + " months";
				serviceList += '\n';
			}
		}
		
		if (serviceList.length() == 0) {
			serviceList = "No services";
		}
		
		if (LoginServlet.activeCustomer.myPackage != null) {
			bill += LoginServlet.activeCustomer.myPackage.getCost();
			packageList = "Package: " + LoginServlet.activeCustomer.listPackages() + ", Cost: $" + 
					df.format(LoginServlet.activeCustomer.myPackage.getCost()) + " Duration: " + 
					LoginServlet.activeCustomer.myPackage.getDuration() + " months";
		}
		
		request.setAttribute("totalCost", df.format(bill1));
		request.setAttribute("subscribedServices", serviceList);
		request.setAttribute("subscribedPackages", packageList);
		request.setAttribute("customer", activeCustomer);
		if(threshold == Double.MAX_VALUE){
			request.setAttribute("placeholder", "");
			request.setAttribute("threshold", "No Threshold Set");
		}
		else{
			request.setAttribute("placeholder", "$");
			request.setAttribute("threshold", df.format(threshold));
		}
		//request.getRequestDispatcher("CustomerViewBill.jsp").forward(request, response);
		
		//System.out.println("THRESHOLD HAS BEEN SET");
		//RequestDispatcher rd = getServletContext().getRequestDispatcher("/CustomerViewBill.jsp");
		//PrintWriter out = response.getWriter();
		
		rd.include(request, response);
	}

}
