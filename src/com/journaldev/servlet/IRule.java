package com.journaldev.servlet;

import javax.mail.MessagingException;
import java.util.Calendar;
public abstract class IRule {
	public static IRule getRules(String ruleType)
	{
		if(ruleType.equals("Commercial")){
			return new CommercialCustomerRules();
		}
		else 
			return new RetailCustomerRules();
	}
    public abstract boolean checkBalance(Customer c);
    public abstract double calculateFine(Customer c, Service s);
	public double calculateFine(Customer c, Package p) {

		return p.getCost() * .01;
	}
}

