package com.journaldev.servlet;

public interface Observer {
	public void update(double bal);
}
