package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CancelPackageServlet
 */
@WebServlet("/CancelPackageServlet")
public class CancelPackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelPackageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String packageList = Representative.masterPackage.listPackages();
		String customerList = LoginServlet.activeCustomer.listPackages();
		request.setAttribute("packageList", packageList);
		request.setAttribute("customerList", customerList);
		request.getRequestDispatcher("CancelPackage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("CancelPackage") != null){
			String packageList = Representative.masterPackage.listPackages();
			String customerList = LoginServlet.activeCustomer.listPackages();
			request.setAttribute("packageList", packageList);
			request.setAttribute("customerList", customerList);
			request.setAttribute("customer", LoginServlet.activeCustomer);
			
			if(request.getParameter("packageOptions") != null){
				String packageName = request.getParameter("packageOptions");
				Package checkPackage;
	
				if(!Representative.allPackages.isEmpty()){
					for(Iterator<Package> iter = Representative.allPackages.listIterator(); iter.hasNext();){
						checkPackage = iter.next();
						
						if(checkPackage.getName().equals(packageName)){
							if(LoginServlet.activeCustomer.removePackage(checkPackage)){
								System.out.println("REMOVING PACKAGE: " + packageName);
								request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
							}
							else {
								System.out.println(packageName + " DOESN'T EXIST");
								RequestDispatcher rd = getServletContext().getRequestDispatcher("/CancelPackage.jsp");
								PrintWriter out = response.getWriter();
								out.println("<font color=red>You are not subscribed to this package.</font>");
								rd.include(request, response);
							}
						}
					}
				}
				else {
					System.out.println("NO PACKAGES");
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/CancelPackage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>No packages to remove.</font>");
					rd.include(request, response);
				}
			}
			else {
				System.out.println("NO CHOICE SELECTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/CancelPackage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please select a package from the list.</font>");
				rd.include(request, response);
			}
		}
	}
}
