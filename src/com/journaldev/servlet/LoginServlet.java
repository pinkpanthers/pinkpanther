package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(
		description = "Login Servlet", 
		urlPatterns = { "/LoginServlet" }, 
		initParams = { 
				@WebInitParam(name = "user", value = "PinkPanthers"), 
				@WebInitParam(name = "password", value = "123")
		})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static Customer activeCustomer = null;
    static service1 Economy = new service1("Internet", 49.99, 12);
    static service1 Elite = new service1("Wirephone", 19.99, 60); 
    static service1 Turbo = new service1("Wireless", 79.99, 24);
    
    //Customer test = new Customer("jwmui", "Jonathan", "Mui", "1234567", 999);
   
	public void init() throws ServletException {
		//we can create DB connection resource here and set it to Servlet context
		if(getServletContext().getInitParameter("dbURL").equals("jdbc:mysql://localhost/mysql_db") &&
				getServletContext().getInitParameter("dbUser").equals("mysql_user") &&
				getServletContext().getInitParameter("dbUserPwd").equals("mysql_pwd"))
		getServletContext().setAttribute("DB_Success", "True");
		else throw new ServletException("DB Connection error");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("customer", activeCustomer);
		request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//get request parameters for userID and password
		String user = request.getParameter("user");
		String pwd = request.getParameter("pwd");
		DecimalFormat df = new DecimalFormat("#.00");
		boolean match = false;

		
		if(user.equals("marketingAdmin") && pwd.equals("admin123")){
			
			String users1 = "";
			String serviceList = "Services: " + '\n';
			String packagelist = "Packages: " + '\n';
			//Create the list of customers in a string
			users1 = GlobalInfo.customerList();
			//create the list of services in a string
			for(Service service : Representative.allServices){
				serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
						+ ", Duration: " + service.getDuration()+" months"+ '\n';
			}
			for(Package pack : Representative.allPackages){
				packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost())
						+ ", Duration: " + pack.getDuration() + " months"+ '\n';
				packagelist += '\t' + "Services: " + pack.listServices() + '\n';
			}
			request.setAttribute("customerList", users1);
			request.setAttribute("serviceList", serviceList);
			request.setAttribute("packageList", packagelist);
			request.getRequestDispatcher("adminPage.jsp").forward(request, response);
		}
		else if(user.equals("customerAdmin") && pwd.equals("admin123")){
			
			String users1 = "";
			String serviceList = "Services: " + '\n';
			String packagelist = "Packages: " + '\n';
			//Create the list of customers in a string
			users1 = GlobalInfo.customerList();
			//create the list of services in a string
			for(Service service : Representative.allServices){
				serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
						+ ", Duration: " + service.getDuration()+" months"+ '\n';
			}
			for(Package pack : Representative.allPackages){
				packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost())
						+ ", Duration: " + pack.getDuration() + " months"+ '\n';
				packagelist += '\t' + "Services: " + pack.listServices() + '\n';
			}
			request.setAttribute("customerList", users1);
			request.setAttribute("serviceList", serviceList);
			request.setAttribute("packageList", packagelist);
			request.getRequestDispatcher("customerAdminPage.jsp").forward(request, response);
		}
		else{
			for(Customer newCustomer : MakeAccountServlet.customerList)
			{
				String userID = newCustomer.getUserName();
				String password = newCustomer.getPassword();
				if(userID.equals(user) && password.equals(pwd)){
					match = true;
					activeCustomer = newCustomer;
				}
			}
			if(match){
				//activeCustomer.addService(testService1);
				//activeCustomer.addService(testService);
				
				//String firstName = activeCustomer.getFirstName();
				request.setAttribute("customer", activeCustomer);
				request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
			}
			else{
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/AccountLogin.html");
				PrintWriter out= response.getWriter();
				out.println("<font color=red>Either user name or password is wrong.</font>");
				rd.include(request, response);
			}
		}
	}

}
