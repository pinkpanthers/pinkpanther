package com.journaldev.servlet;

//import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class EconomyServlet
 */
@WebServlet("/EconomyServlet")
public class EconomyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EconomyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//PrintWriter out= response.getWriter();
		if(!LoginServlet.activeCustomer.contains(LoginServlet.Economy)){
			LoginServlet.activeCustomer.addService(LoginServlet.Economy);
			//request.setAttribute("customer", LoginServlet.activeCustomer);
			//request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
		}
		//else
			//out.println("<font color=red>Already Subscribed</font>");
			
			request.setAttribute("customer", LoginServlet.activeCustomer);
			request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
			
	}

}
