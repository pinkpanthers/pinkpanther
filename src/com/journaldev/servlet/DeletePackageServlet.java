package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeletePackageServlet
 */
@WebServlet("/DeletePackageServlet")
public class DeletePackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletePackageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String packageName = request.getParameter("packageName");
		String users1 = "";
		String serviceList = "Services: " + '\n';
		String packagelist = "Packages: " + '\n';
		DecimalFormat df = new DecimalFormat("#.00");
		//Create the list of customers in a string
		for(Customer newCustomer : MakeAccountServlet.customerList)
		{
			users1 += "Username: ";
			users1 += newCustomer.getUserName() + '\n';
			users1 += "Services: ";
			users1 += newCustomer.listServices() + '\n';
			users1 += "Packages: ";
			users1 += newCustomer.listPackages() + '\n';
			users1 += '\n';
		}
		//create the list of services in a string
		for(Service service : Representative.allServices){
			serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
					+ ", Duration: " + service.getDuration()+" months"+ '\n';
		}
		for(Package pack : Representative.allPackages){
			packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost())
					+ ", Duration: " + pack.getDuration() + " months"+ '\n';
			packagelist += '\t' + "Services: " + pack.listServices() + '\n';
		}
		request.setAttribute("customerList", users1);
		request.setAttribute("serviceList", serviceList);
		request.setAttribute("packageList", packagelist);
		
		if(Representative.allPackages.size() == 0){
			System.out.println("NO PACKAGES");
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
			PrintWriter out = response.getWriter();
			out.println("<font color=red>No packages to delete.</font>");
			rd.include(request, response);
		}
		else{
			if(packageName.length() != 0){
				Package checkPackage;
				
				for(Iterator<Package> iter = Representative.allPackages.listIterator(); iter.hasNext();){
					checkPackage = iter.next();
					
					if(checkPackage.getName().equals(packageName)){
						
						if (Representative.allPackages.remove(checkPackage)) {
							for(Customer checkCustomer : MakeAccountServlet.customerList)
							{
								if(checkCustomer.myPackage.equals(checkPackage)){
									checkCustomer.removePackage(checkPackage);
								}
							}
							System.out.println("DELETING PACKAGE: " + packageName);
							packagelist = "Packages: " + '\n';
							for(Package pack : Representative.allPackages){
								packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost())
										+ ", Duration: " + pack.getDuration() + " months"+ '\n';
								packagelist += '\t' + "Services: " + pack.listServices() + '\n';
							}
							request.setAttribute("packageList", packagelist);
							request.getRequestDispatcher("adminPage.jsp").forward(request, response);
							break;
						}
					}
					else {
						System.out.println(packageName + " DOESN'T EXIST");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>Please input a valid package to delete.</font>");
						rd.include(request, response);
					}
				}
			}
			else {
				System.out.println("NO PACKAGE NAME INPUTTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input a valid package to delete.</font>");
				rd.include(request, response);
			}
		}
	}
}
