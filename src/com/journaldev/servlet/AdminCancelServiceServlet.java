package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminCancelServiceServlet
 */
@WebServlet("/AdminCancelServiceServlet")
public class AdminCancelServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminCancelServiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String serviceList = Representative.masterService.listServices();
		String subscribedList = LoginServlet.activeCustomer.listServices();
		String customerList = LoginServlet.activeCustomer.listCustomerServices();
		request.setAttribute("serviceList", serviceList);
		request.setAttribute("subscribedList", subscribedList);
		request.setAttribute("customerList", customerList);
		request.getRequestDispatcher("adminCancelService.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("adminCancelService") != null){
			String serviceList = Representative.masterService.listServices();
			String subscribedList = LoginServlet.activeCustomer.listServices();
			String customerList = LoginServlet.activeCustomer.listCustomerServices();
			request.setAttribute("serviceList", serviceList);
			request.setAttribute("subscribedList", subscribedList);
			request.setAttribute("customerList", customerList);
			request.setAttribute("customer", LoginServlet.activeCustomer);
			
			if(request.getParameter("serviceOptions") != null){
				String serviceName = request.getParameter("serviceOptions");
				Service checkService;
			
				if(!LoginServlet.activeCustomer.basket.isEmpty()){
					for(Iterator<Service> iter = LoginServlet.activeCustomer.basket.listIterator(); iter.hasNext();){
						checkService = iter.next();
						
						if(checkService.getName().equals(serviceName)){
							if (LoginServlet.activeCustomer.removeService(checkService)) {
								System.out.println("REMOVING SERVICE: " + serviceName);
								request.getRequestDispatcher("AccountOverviewRep.jsp").forward(request, response);
							}
							else {
								System.out.println(serviceName + " DOESN'T EXIST");
								RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminCancelService.jsp");
								PrintWriter out = response.getWriter();
								out.println("<font color=red>You are not subscribed to this service.</font>");
								rd.include(request, response);
							}
						}
						/*else {
							System.out.println(serviceName + " DOESN'T EXIST");
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminCancelService.jsp");
							PrintWriter out = response.getWriter();
							out.println("<font color=red>Service doesn't exist.</font>");
							rd.include(request, response);
						}*/
					}
				}
				else {
					System.out.println(serviceName + " DOESN'T EXIST");
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminCancelService.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>You are not subscribed to any services.</font>");
					rd.include(request, response);
				}
			}
			else {
				System.out.println("NO CHOICE SELECTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminCancelService.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please select a service from the list.</font>");
				rd.include(request, response);
			}
		}
	}
}
