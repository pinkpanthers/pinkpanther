package com.journaldev.servlet;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

public class NotifyRetailCustomer extends NotifyCustomer {
    IRule retail = new RetailCustomerRules();
    
    
	public void notifyBalance(Customer c) {
		Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.port", 465);
	    props.put("mail.smtp.ssl.enable", true);
	    Authenticator authenticator = null;
	    
	    props.put("mail.smtp.auth", true);
        authenticator = new Authenticator() {
            private PasswordAuthentication pa = new PasswordAuthentication("cse110pinkpanthers@gmail.com", "Pinkpanthers1");
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return pa;
            }
        };
	    
	    Session session = Session.getInstance(props, authenticator);
	    MimeMessage msg = new MimeMessage(session);
	    try {
	        msg.setFrom("me@example.com");
	        msg.setRecipients(Message.RecipientType.TO,
	                          c.getEmail());
	        System.out.println(c.getEmail());
	        msg.setSubject("Telegroup Communiications Notification");
	        msg.setSentDate(new Date());
	        msg.setText(c.getFirstName() +", your bill has exceeded the threshold you have set in your account!\n");
	        Transport.send(msg);
	    } catch (MessagingException mex) {
	        System.out.println("send failed, exception: " + mex);
	    }

	}

}
