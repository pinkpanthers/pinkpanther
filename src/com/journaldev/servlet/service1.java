package com.journaldev.servlet;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;


public class service1 implements Service {
	
	String name;
	double cost;
	int duration;
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date subscriptionDate;
	//static List<Service> allServices = new ArrayList<Service>();
	//Date lateDate = new Date
	Calendar calendar;
	public service1(String name, double cost, int duration){
		this.name = name;
		this.cost = cost;
		this.duration = duration;
		Representative.allServices.add(this);
		this.calendar = Calendar.getInstance();
		this.subscriptionDate = new Date();
	};
	public service1(){}
	public String getName() {
	
		// TODO Auto-generated method stub
		return this.name;
	}
		
	public Date getSubscriptionDate(){
		return this.subscriptionDate;
	}
	public double getCost() {
		// TODO Auto-generated method stub
		return this.cost;
	}
	public void setDate(Calendar d){
		this.calendar = d;
	}
	public Calendar getCalendar(){
		return (Calendar)this.calendar.clone();
	}
	public String listServices(){
		String serviceList = "";
		DecimalFormat df = new DecimalFormat("#.00");
		int counter = 0;

		for(Service services: Representative.allServices){
			if(counter%3==0)
			{
				if(counter == 0)
					serviceList = serviceList 
								+ "<div class = 'form-group'><div class='row'>";
				else
					serviceList = serviceList 
								+ "</div><div class='row'>";
			}
			serviceList = serviceList 
					+ "<div class='col-md-4 col-sm-4'>"
                	+ "<div class='panel panel-info'>"
                	+ "<div class='panel-heading'>" + services.getName() + "</div>"
                	+ "<div class='panel-body'><h4>"+ "$" + df.format(services.getCost()) + "</h4>"
                	+ "<h4>"+ "Duration: "+ services.getDuration() + " months" +"</h4></div>"
                	+ "<div class='panel-footer'>"
                	+ 	"<div class ='radio'>"
                	+ 		"<label><input type='radio' name='serviceOptions' value='" +services.getName()+ "'/> Select </label>"
                	+ 	"</div></div>"
                	+ "</div></div>";		
			counter++;
		}
		serviceList += "</div></div>";

		return serviceList;
	}
	@Override
	public int getDuration() {
		return this.duration;
	}

	
	/* double calculateFee(){
	 * if(localDate > lateDate)
	 * {
	 *		calculate fee
	 * }
	 * else
	 * return this.cost;
	 * 
	 * 
	 */

}
