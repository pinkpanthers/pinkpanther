package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("customer", LoginServlet.activeCustomer);
		request.getRequestDispatcher("AccountOverviewRep.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("adminModify") != null){
			String users1 = "";
			for(Customer allCustomer : MakeAccountServlet.customerList)
			{
				users1 += "Username: ";
				users1 += allCustomer.getUserName() + '\n';
				users1 += "Services: ";
				users1 += allCustomer.listServices() + '\n';
				users1 += "Packages: ";
				users1 += allCustomer.listPackages() + '\n';
				users1 += '\n';
			}
			request.setAttribute("customerList", users1);
			
			if(MakeAccountServlet.customerList.size() == 0){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>No customer accounts to modify.</font>");
				rd.include(request, response);
			}
			else{
				for(Customer newCustomer : MakeAccountServlet.customerList)
				{
					String userID = newCustomer.getUserName();
					String user = request.getParameter("user");
					System.out.println(user);
					
					if(userID.equals(user)){
						LoginServlet.activeCustomer = newCustomer;
						request.setAttribute("customer", LoginServlet.activeCustomer);
						request.getRequestDispatcher("AccountOverviewRep.jsp").forward(request, response);
					}
					else{
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>Please input a valid username to modify an account.</font>");
						rd.include(request, response);
					}
				}
			}
		}
	}
}
