package com.journaldev.servlet;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class Package {
	private ArrayList<Service> services;
	private String name;
	private double cost;
	private double rate;
	private double fixed;
	private int duration;

	public Package (String name, double rate, double fixed, int duration) {
		services = new ArrayList<Service>();
		this.name = name;
		this.cost = 0.00;
		this.rate = rate/100;
		this.fixed = fixed;
		this.duration = duration;
		Representative.allPackages.add(this);
	}
	public Package() {}
	
	public String getName() {
		return this.name;
	}
	
	public double getCost() {
		return this.cost;
	}
	
	public void updateCost() {
		this.cost = 0.0;
		if (services.size() != 0) {
		
			for (Service i : services) {
				System.out.println("ADDING COST OF " + i.getName() + " ($" + i.getCost() + ") TO PACKAGE");
				this.cost += i.getCost();
			}
			if (this.cost*(1-this.rate) - fixed > 0)
				this.cost = this.cost*(1-this.rate) - fixed;
		}
	}
	
	
	public void addService (Service service) {
		if (!services.contains(service))
			services.add(service);
		this.updateCost();
	}
	
	public void removeService (Service service) {
		if (services.contains(service))
			services.remove(service);
		this.updateCost();
	}
	
	public String listPackages(){
		
		String packageList = "";
		int counter = 0;
		DecimalFormat df = new DecimalFormat("#.00");
		
		for(Package packages: Representative.allPackages){
			int countService = 0;
			
			if(counter%3==0)
			{
				if(counter == 0)
					packageList = packageList 
								+ "<div class = 'form-group'><div class='row'>";
				else
					packageList = packageList 
								+ "</div><div class='row'>";
			}
			packageList = packageList 
					+ "<div class='col-md-4 col-sm-4'>"
                	+ "<div class='panel panel-info'>"
					//Package Name
                	+ "<div class='panel-heading'>" + packages.getName() + "</div>"
                	//Package Description
                	+ "<div class='panel-body'><h4>"+ "Duration: "+ packages.getDuration() + " months" +"</h4>";
                	//+ "<ul class='list-unstyled' style='line-height: 2'>";
			while(countService < packages.services.size()){
				packageList += "<li>" + packages.services.get(countService).getName() + "</li>";
				countService++;
			}
			//packageList += "</ul>";
			packageList +="<h4>"+ "$" + df.format(packages.getCost())+ "</h4>"
                	+ "</div>"
                	+ "<div class='panel-footer'>"
                	+ 	"<div class ='radio'>"
                	+ 		"<label><input type='radio' name='packageOptions' value='" +packages.getName()+ "'/> Select </label>"
                	+ 	"</div></div>"
                	+ "</div></div>";		
			counter++;
		}
		packageList += "</div></div>";
		

		return packageList;
		
		
	}
	public boolean contains(Service service){
		if(!services.isEmpty()){
			for(Iterator<Service> iter = services.listIterator(); iter.hasNext();){
				Service checkService = iter.next();
				if(checkService.getName() == service.getName()){
					return true;
				}
			}
		}
		return false;
	}
	
	public String listServices() {
		String serviceList = "";
		int counter = 0;
		for(Service i : services){
			serviceList += i.getName();
			counter++;
			if (counter < services.size())
				serviceList += ", ";
		}
		return serviceList;
	}
	public int getDuration() {
		return this.duration;
	}
}
