package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreatePackageServlet
 */
@WebServlet("/CreatePackageServlet")
public class CreatePackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePackageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DecimalFormat df = new DecimalFormat("#.00");
		String users1 = "";
		String serviceList = "Services: " + '\n';
		String packagelist = "Packages: " + '\n';
		String error = "";
		//Create the list of customers in a string
		for(Customer newCustomer : MakeAccountServlet.customerList)
		{
			users1 += "Username: ";
			users1 += newCustomer.getUserName() + '\n';
			users1 += "Services: ";
			users1 += newCustomer.listServices() + '\n';
			users1 += "Packages: ";
			users1 += newCustomer.listPackages() + '\n';
			users1 += '\n';
		}
		//create the list of services in a string
		for(Service service : Representative.allServices){
			serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost()) +
					", Duration: " + service.getDuration() + " months" + '\n';
		}
		for(Package pack : Representative.allPackages){
			packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost()) + 
					", Duration: " + pack.getDuration() + " months" + '\n';
			packagelist += '\t' + " Services: " + pack.listServices() + '\n';
		}
		request.setAttribute("customerList", users1);
		request.setAttribute("serviceList", serviceList);
		request.setAttribute("packageList", packagelist);
		
		if(request.getParameter("createPackage") != null){
			//System.out.println("ENTERING CREATEPACKAGE METHOD");
			String packageName = request.getParameter("packageName");
			String discountFixed = request.getParameter("Fixed");
			String discountPercent = request.getParameter("Percent");
			String durationS = request.getParameter("duration");
			String service1 = "";
			String service2 = "";
			String service3 = "";
			service1 = request.getParameter("service1");
			service2 = request.getParameter("service2");
			service3 = request.getParameter("service3");
			
			if(packageName.length() == 0){
				System.out.println("NO PACKAGE NAME INPUTTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input a package name.</font>");
				rd.include(request, response);
				
				/*error = "<div class='alert alert-danger' role='alert'>"
						 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
						 +		"<span class='sr-only'>Error:</span>"
						 +		"Please input a package name."
						 + "</div>";
				request.setAttribute("errorMessage", error);
				request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
			}
			else if(service1.length() == 0 && service2.length() == 0 && service3.length() == 0){
				System.out.println("NO SERVICE INPUTTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input at least 1 service to add to the package.</font>");
				rd.include(request, response);
				
				/*error = "<div class='alert alert-danger' role='alert'>"
						 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
						 +		"<span class='sr-only'>Error:</span>"
						 +		"Please input at least 1 service to add to the package."
						 + "</div>";
				request.setAttribute("errorMessage", error);
				request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
			}
			else if (discountFixed.length() == 0 || discountPercent.length() == 0 || durationS.length() == 0){
				System.out.println("MISSING FIELDS");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input all discount fields and duration.</font>");
				rd.include(request, response);
				
				/*error = "<div class='alert alert-danger' role='alert'>"
						 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
						 +		"<span class='sr-only'>Error:</span>"
						 +		"Please input all discount fields and duration."
						 + "</div>";
				request.setAttribute("errorMessage", error);
				request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
			}
			else{
				boolean packageNameCheck = true; //false if name exists
				//Check if package name exists
				for(Package packages : Representative.allPackages){
					if(packages.getName().equals(packageName)){
						packageNameCheck = false;
						System.out.println("PACKAGE NAME ALREADY EXISTS");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>Package name already exists. Please choose a different package name.</font>");
						rd.include(request, response);
						
						/*error = "<div class='alert alert-danger' role='alert'>"
									 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
									 +		"<span class='sr-only'>Error:</span>"
									 +		"Package name already exists. Please choose a different package name."
									 + "</div>";
						request.setAttribute("errorMessage", error);
						packageNameCheck = false;
						request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
					}
				}
				
				//package name check pass
				if(packageNameCheck){
					Service potentialService;
					
					if(discountFixed.length() == 0 || !discountFixed.matches("\\d+")){
						System.out.println("NO FIXED DISCOUNT INPUTTED");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>Please input a valid fixed discount price.</font>");
						rd.include(request, response);
					}
					else if(discountPercent.length() == 0 || !discountPercent.matches("\\d+")){
						System.out.println("NO PERCENT DISCOUNT INPUTTED");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>Please input a valid percent discount.</font>");
						rd.include(request, response);
					}
					else if(durationS.length() == 0 || !durationS.matches("\\d+")){
						System.out.println("NO DURATION INPUTTED");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>Please input a valid duration for the package.</font>");
						rd.include(request, response);
					}
					else if((GlobalInfo.isService(service1) == null && service1.length() !=0) 
						|| (GlobalInfo.isService(service2) == null && service2.length() !=0)
							|| (GlobalInfo.isService(service3) == null && service3.length() !=0)){
						System.out.println("INVALID SERVICE INPUT");
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
						PrintWriter out = response.getWriter();
						out.println("<font color=red>One of your service inputs is invalid.</font>");
						rd.include(request, response);
						
						/*error = "<div class='alert alert-danger' role='alert'>"
								 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
								 +		"<span class='sr-only'>Error:</span>"
								 +		"One of your service inputs is invalid."
								 + "</div>";
						request.setAttribute("errorMessage", error);
						request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
						
					}
					else{
						Double fixed = Double.parseDouble(discountFixed);
						Double percent = Double.parseDouble(discountPercent);
						int duration = Integer.parseInt(durationS);
						
						Package newPackage = new Package(packageName, percent, fixed, duration); // Name, rate, fixed
						Package changedPackage = Representative.allPackages.get(Representative.allPackages.indexOf(newPackage));
						if(service1 != null){
							potentialService = GlobalInfo.isService(service1);
							if(potentialService != null)
								changedPackage.addService(potentialService);
						}
						if(service2 != null){
							potentialService = GlobalInfo.isService(service2);
							if(potentialService != null)
								changedPackage.addService(potentialService);
						}
						if(service3 != null){
							potentialService = GlobalInfo.isService(service3);
							if(potentialService != null)
								changedPackage.addService(potentialService);
						}
						
						packagelist = "Packages: " + '\n';
						for(Package pack : Representative.allPackages){
							packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost()) + 
									", Duration: " + pack.getDuration() + " months" + '\n';
							packagelist += '\t' + " Services: " + pack.listServices() + '\n';
						}
						request.setAttribute("packageList", packagelist);
						request.getRequestDispatcher("adminPage.jsp").forward(request, response);
					}
				} 
			}
		}
		else if(request.getParameter("modifyPackage") != null){
			//System.out.println("ENTERING MODIFYPACKAGE METHOD");
			String packageToChange = "";
			String serviceToAdd = "";
			Package toChange;
			Service toAdd;
			packageToChange = request.getParameter("packageChange");
			serviceToAdd = request.getParameter("serviceAdd");
			
			if(packageToChange.length() == 0 || serviceToAdd.length() == 0){
				System.out.println("MISSING FIELDS");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input all fields to modify package.</font>");
				rd.include(request, response);
				
				/*error = "<div class='alert alert-danger' role='alert'>"
						 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
						 +		"<span class='sr-only'>Error:</span>"
						 +		"Please input all fields to modify package."
						 + "</div>";
				request.setAttribute("errorChangingPackage", error);
				request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
			}
			else if(GlobalInfo.isPackage(packageToChange) == null || GlobalInfo.isService(serviceToAdd) == null){
				System.out.println("INVALID PACKAGE/SERVICE NAME");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input valid package or service name.</font>");
				rd.include(request, response);
				
				/*error = "<div class='alert alert-danger' role='alert'>"
						 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
						 +		"<span class='sr-only'>Error:</span>"
						 +		"Please input valid package or service name."
						 + "</div>";
				request.setAttribute("errorChangingPackage", error);
				request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
			}
			else{
				toChange = GlobalInfo.isPackage(packageToChange);
				toAdd = GlobalInfo.isService(serviceToAdd);
				if(toChange.contains(toAdd)){
					System.out.println("SERVICE ALREADY EXISTS IN PACKAGE");
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Service already exists in package.</font>");
					rd.include(request, response);
					
					/*error = "<div class='alert alert-danger' role='alert'>"
							 + 		"<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>"
							 +		"<span class='sr-only'>Error:</span>"
							 +		"Service already exists in package."
							 + "</div>";
					request.setAttribute("errorChangingPackage", error);
					request.getRequestDispatcher("adminPage.jsp").forward(request, response);*/
				}
				else{
					toChange.addService(toAdd);
					packagelist = "Packages: " + '\n';
					for(Package pack : Representative.allPackages){
						packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost()) + 
								", Duration: " + pack.getDuration() + " months"+ '\n';
						packagelist += '\t' + "Services: " + pack.listServices() + '\n';
					}
					request.setAttribute("packageList", packagelist);
					request.getRequestDispatcher("adminPage.jsp").forward(request, response);
				}
			}
		}
	}
}


