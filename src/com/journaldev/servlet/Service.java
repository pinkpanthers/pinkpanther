package com.journaldev.servlet;

import java.util.Date;

public interface Service {
	
	public String getName();
	public double getCost();
	public int getDuration();
	public Date getSubscriptionDate();
}
