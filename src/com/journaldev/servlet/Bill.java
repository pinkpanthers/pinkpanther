package com.journaldev.servlet;

import java.util.ArrayList;


public class Bill implements Subject{
	private double balanceChange;
	
	private ArrayList<Observer> observers;
	
	public Bill() {
		observers = new ArrayList<Observer>();
	}
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
		
	}
	@Override
	public void removeObserver(Observer o) {
		int i = observers.indexOf(o);
		if (i >= 0){
			observers.remove(i);
		}
		
	}

	@Override
	public void notifyObservers() {
		for(int i = 0; i < observers.size(); i++){
		Observer observer = (Observer)observers.get(i);
		observer.update(balanceChange);
		//System.out.println(balanceChange);
		}
	}
	
	public void billChanged(){
		notifyObservers();
	}
	
	public void addService(Service newService){
		balanceChange = newService.getCost();
		billChanged();
	}
	
	public void addPackage(Package newPackage){
		balanceChange = newPackage.getCost();
		billChanged();
	}
	public void removeService(Service cancelService){
		balanceChange = 0-cancelService.getCost();
		billChanged();
	}
	public void removePackage(Package cancelPackage){
		balanceChange = 0-cancelPackage.getCost();
		billChanged();
	}
	public void addFee(double fee){
		balanceChange = fee;
		billChanged();
	}
	

}
