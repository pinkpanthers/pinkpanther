package com.journaldev.servlet;

public abstract class NotifyCustomer { 
	public IRule rules;
    public abstract void notifyBalance(Customer c);
}
