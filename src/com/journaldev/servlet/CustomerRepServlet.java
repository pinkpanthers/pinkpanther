package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CustomerRepServlet
 */
@WebServlet("/CustomerRepServlet")
public class CustomerRepServlet extends HttpServlet {
	DecimalFormat df = new DecimalFormat("#.00");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerRepServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String users1 = "";
		String serviceList = "Services: " + '\n';
		String packagelist = "Packages: " + '\n';
		//Create the list of customers in a string
		users1 = GlobalInfo.customerList();
		//create the list of services in a string
		for(Service service : Representative.allServices){
			serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
					+ ", Duration: " + service.getDuration()+" months"+ '\n';
		}
		for(Package pack : Representative.allPackages){
			packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost())
					+ ", Duration: " + pack.getDuration() + " months"+ '\n';
			packagelist += '\t' + "Services: " + pack.listServices() + '\n';
		}
		request.setAttribute("customerList", users1);
		request.setAttribute("serviceList", serviceList);
		request.setAttribute("packageList", packagelist);
		request.getRequestDispatcher("customerAdminPage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("registerAdmin") != null){
			//get request parameters for userID and password
			String user = null;
			String pwd = null;
			String users1 = "";
			for(Customer allCustomer : MakeAccountServlet.customerList)
			{
				users1 += "Username: ";
				users1 += allCustomer.getUserName() + '\n';
				users1 += "Services: ";
				users1 += allCustomer.listServices() + '\n';
				users1 += "Packages: ";
				users1 += allCustomer.listPackages() + '\n';
				users1 += '\n';
			}
			request.setAttribute("customerList", users1);
		
			user = request.getParameter("companyuser");
			pwd = request.getParameter("companypwd");
			System.out.println("I'M IN THE COMMERCIAL CASE");
			
			if (user.length() == 0 && pwd.length() == 0) {
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input a username and a password.</font>");
				rd.include(request, response);
			}

			else {
				if (user.length() == 0) {
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Please input a username.</font>");
					rd.include(request, response);
				}
				
				if (pwd.length() == 0) {
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Please input a password.</font>");
					rd.include(request, response);
				}
				
				if (pwd.length() < 6 || !pwd.matches(".*\\d+.*")) {
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Password too weak. Please choose a password that is at least 6 characters long and contains at least one number.</font>");
					rd.include(request, response);
				}
				
				
				String companyName = request.getParameter("companyName");
				String companyBillName = request.getParameter("billName");
				String companyBillEmail = request.getParameter("billEmail");
				String companyBillNumber = request.getParameter("billNumber");
				
				if (companyName.length() == 0) {
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Please input a company name.</font>");
					rd.include(request, response);
				}
				
				if(companyBillName.length() == 0 || companyBillEmail.length() == 0 || companyBillNumber.length() == 0){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/customerAdminPage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Please complete all fields for billing information.</font>");
					rd.include(request, response);
				}
				
				if (pwd.length() >= 6 && pwd.matches(".*\\d+.*") && companyName.length() != 0) {
					String firstname = request.getParameter("companyfirstname");
					String lastname = request.getParameter("companylastname");
					String username = request.getParameter("companyuser");
					String password = request.getParameter("companypwd");
					String email = request.getParameter("companyemail");
					String phone = request.getParameter("companyphone");
					System.out.println(firstname);
					System.out.println(lastname);
					System.out.println(username);
					System.out.println(email);
					System.out.println(phone);
					Customer newCustomer = new CommercialCustomer(companyName, username, firstname,
							lastname, password, email, companyBillName,
							companyBillEmail, companyBillNumber);
					MakeAccountServlet.customerList.add(newCustomer);
					users1 = "";
					String serviceList = "Services: " + '\n';
					String packagelist = "Packages: " + '\n';
					//Create the list of customers in a string
					for(Customer allCustomer : MakeAccountServlet.customerList)
					{
						users1 += "Username: ";
						users1 += allCustomer.getUserName() + '\n';
						users1 += "Services: ";
						users1 += allCustomer.listServices() + '\n';
						users1 += "Packages: ";
						users1 += allCustomer.listPackages() + '\n';
						users1 += '\n';
					}
					//create the list of services in a string
					for(Service service : Representative.allServices){
						serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
								+ ", Duration: " + service.getDuration()+" months"+ '\n';
					}
					for(Package pack : Representative.allPackages){
						packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost())
								+ ", Duration: " + pack.getDuration() + " months"+ '\n';
						packagelist += '\t' + "Services: " + pack.listServices() + '\n';
					}
					request.setAttribute("customerList", users1);
					request.setAttribute("serviceList", serviceList);
					request.setAttribute("packageList", packagelist);
					request.getRequestDispatcher("customerAdminPage.jsp").forward(request, response);
				}
			}
		}
	}
}
