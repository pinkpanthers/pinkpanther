package com.journaldev.servlet;

import java.util.List;

public class CustomerServiceRepresentative extends Representative {

	public CustomerServiceRepresentative(String userName, String firstName,
			String lastName, String password, int id) {
		super(userName, firstName, lastName, password, id);
		// TODO Auto-generated constructor stub
	}
	
	public boolean addService(Service service, Customer c)
	{
		return true;
	}

	public boolean cancelService(Service service, Customer c)
	{
		return true;
	}
	
	public boolean addPackage(List<Package> packages, Customer c)
	{
		return true;
	}
	
	public boolean cancelPackage(List<Package> packages, Customer c)
	{
		return true;
	}

}
