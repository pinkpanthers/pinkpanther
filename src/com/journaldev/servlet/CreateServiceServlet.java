package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreateServiceServlet
 */
@WebServlet("/CreateServiceServlet")
public class CreateServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String serviceName = request.getParameter("serviceName");
		String priceS = request.getParameter("price");
		Double priceD = null;
		/*if(priceS.length() != 0 && priceS.matches("\\d+"))
			priceD = Double.parseDouble(priceS);*/
		DecimalFormat df = new DecimalFormat("#.00");
		String durationS = request.getParameter("duration");
		int duration = 0;
		/*if(durationS.length() != 0 && durationS.matches("\\d+"))
			duration = Integer.parseInt(durationS);*/
		
		String users1 = "";
		String serviceList = "Services: " + '\n';
		String packagelist = "Packages: " + '\n';
		//Create the list of customers in a string
		for(Customer newCustomer : MakeAccountServlet.customerList)
		{
			users1 += "Username: ";
			users1 += newCustomer.getUserName() + '\n';
			users1 += "Services: ";
			users1 += newCustomer.listServices() + '\n';
			users1 += "Packages: ";
			users1 += newCustomer.listPackages() + '\n';
			users1 += '\n';
		}
		//create the list of services in a string
		for(Service service : Representative.allServices){
			serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
					+ ", Duration: " + service.getDuration()+" months"+ '\n';
		}
		for(Package pack : Representative.allPackages){
			packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost()) + '\n';
			packagelist += '\t' + "Services: " + pack.listServices() + '\n';
		}
		request.setAttribute("customerList", users1);
		request.setAttribute("serviceList", serviceList);
		request.setAttribute("packageList", packagelist);
		
		if(request.getParameter("createService") != null){
			if(serviceName.length() == 0){
				System.out.println("NO SERVICE NAME INPUTTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input a service name.</font>");
				rd.include(request, response);
			}
			else if(priceS.length() == 0 || !priceS.matches("\\d+")){
				System.out.println("NO PRICE INPUTTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input a price for the service.</font>");
				rd.include(request, response);
			}
			else if(durationS.length() == 0 || !durationS.matches("\\d+")){
				System.out.println("NO DURATION INPUTTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminPage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please input a duration for the service.</font>");
				rd.include(request, response);
			}
			else{
				priceD = Double.parseDouble(priceS);
				duration = Integer.parseInt(durationS);
				service1 newService = new service1(serviceName, priceD, duration);
				users1 = "";
				serviceList = "Services: " + '\n';
				packagelist = "Packages: " + '\n';
				//Create the list of customers in a string
				for(Customer newCustomer : MakeAccountServlet.customerList)
				{
					users1 += "Username: ";
					users1 += newCustomer.getUserName() + '\n';
					users1 += "Services: ";
					users1 += newCustomer.listServices() + '\n';
					users1 += "Packages: ";
					users1 += newCustomer.listPackages() + '\n';
					users1 += '\n';
				}
				//create the list of services in a string
				for(Service service : Representative.allServices){
					serviceList += "Name: " + service.getName() + ", Price: $" + df.format(service.getCost())
							+ ", Duration: " + service.getDuration()+" months"+ '\n';
				}
				for(Package pack : Representative.allPackages){
					packagelist += "Name: " + pack.getName() + ", Price: $" + df.format(pack.getCost()) + '\n';
					packagelist += '\t' + "Services: " + pack.listServices() + '\n';
				}
				request.setAttribute("customerList", users1);
				request.setAttribute("serviceList", serviceList);
				request.setAttribute("packageList", packagelist);
				request.getRequestDispatcher("adminPage.jsp").forward(request, response);
			}
		}
	}
}