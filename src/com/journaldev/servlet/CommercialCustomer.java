package com.journaldev.servlet;

//requirements for Commercial Customer:
//Company Name, responsible party (person), address and phone number of billing department and name of responsible person
public class CommercialCustomer extends Customer {
	String companyName;
	String billingName;
	String billingEmail;
	String billingNumber;
	

	
	public CommercialCustomer(String compName, String userName, String firstName,
			String lastName, String password, String email, String billName,
			String billEmail, String billNumber) {
		//Super class constructer fills in the information for the responsible party
		super(userName, firstName, lastName, password, email, "Commercial");
		companyName = compName;
		
		//These fill in the info for the billing department
		companyName = compName;
		billingName = billName;
		billingEmail = billEmail;
		billingNumber = billNumber;

	}
	public void setCompanyName( String name){
		companyName = name;
	}
	public void setBillingName( String name){
		billingName = name;
	}
	public void setBillingNumber( String num){
		billingNumber = num;
	}
	public String getCompanyName()
	{
		return this.companyName;
	}
	
	public String getBillingName()
	{
		return this.billingName;
	}
	
	public String getBillingEmail(){
		return this.billingEmail;	
	}
	
	public String getBillingNumber()
	{
		return this.billingNumber;
	}

}
