package com.journaldev.servlet;

import java.util.Iterator;

public class GlobalInfo {

	static Service isService (String serviceName)
	{
		Service checkService;
		if(!Representative.allServices.isEmpty()){
			for(Iterator<Service> iter = Representative.allServices.listIterator(); iter.hasNext();){			
				checkService = iter.next();
				if(checkService.getName().equals(serviceName))
					return checkService;								
			}			
		}
		return null;
	}
	
	static Package isPackage (String packageName)
	{
		Package checkPackage;
		if(!Representative.allPackages.isEmpty())
		{
			for(Iterator<Package> iter = Representative.allPackages.listIterator(); iter.hasNext();)
			{
				checkPackage = iter.next();
				if(checkPackage.getName().equals(packageName))
					return checkPackage;
			}
		}
		return null;
	}
	static String customerList(){
		String users1 = "";
		for(Customer newCustomer : MakeAccountServlet.customerList)
		{
			users1 += "Username: ";
			users1 += newCustomer.getUserName() + '\n';
			users1 += "Services: ";
			users1 += newCustomer.listServices() + '\n';
			users1 += "Packages: ";
			users1 += newCustomer.listPackages() + '\n';
			users1 += '\n';
		}
		return users1;
	}
}
