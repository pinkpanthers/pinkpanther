package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class addPackageServlet
 */
@WebServlet("/addPackageServlet")
public class addPackageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addPackageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String packageList = Representative.masterPackage.listPackages();
		String customerList = LoginServlet.activeCustomer.listPackages();
		request.setAttribute("packageList", packageList);
		request.setAttribute("customerList", customerList);
		request.getRequestDispatcher("addPackage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("AddPackage") != null){
			String packageList = Representative.masterPackage.listPackages();
			String customerList = LoginServlet.activeCustomer.listPackages();
			request.setAttribute("packageList", packageList);
			request.setAttribute("customerList", customerList);
			request.setAttribute("customer", LoginServlet.activeCustomer);
			
			if(request.getParameter("packageOptions") != null){
				String packageName = request.getParameter("packageOptions");
				Package checkPackage;
	
				if(!Representative.allPackages.isEmpty()){
					for(Iterator<Package> iter = Representative.allPackages.listIterator(); iter.hasNext();){
						checkPackage = iter.next();
						
						if(checkPackage.getName().equals(packageName)){
							if (LoginServlet.activeCustomer.addPackage(checkPackage)){
								System.out.println("ADDING PACKAGE: " + packageName);
								request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
							}
							else {
								System.out.println("ALREADY SUBSCRIBED TO A PACKAGE");
								RequestDispatcher rd = getServletContext().getRequestDispatcher("/addPackage.jsp");
								PrintWriter out = response.getWriter();
								out.println("<font color=red>You must remove your subscribed package before adding a new one.</font>");
								rd.include(request, response);
							}
						}
					}
				}
				else {
					System.out.println("NO PACKAGES");
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/addPackage.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>No packages to add.</font>");
					rd.include(request, response);
				}
			}
			else {
				System.out.println("NO CHOICE SELECTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/addPackage.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please select a package from the list.</font>");
				rd.include(request, response);
			}
		}
	}
}
