package com.journaldev.servlet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Date;


public class Customer extends User implements Observer{
	
	Bill bill = new Bill();
	private double balance;
	double threshold = Double.MAX_VALUE;
	IRule rule;
	int id;
	String userName;
	String firstName;
	String lastName;
	String password;
	String email;
	String accountType;
	Package myPackage;
	double fee = 150.0;
	List<Service> basket = new ArrayList<Service>();
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	public Customer(){
		
	}
	public Customer(String userName, String firstName, String lastName, String password,String email, String rule){
	this.userName = userName;
	this.firstName = firstName;
	this.lastName = lastName;
	this.password = password;
	this.email = email;
	this.accountType = rule;
	this.rule = IRule.getRules(rule);
	this.bill.registerObserver(this);
	this.myPackage = null;
	}
	
	public String listPackages(){
		if (myPackage != null)
			return myPackage.getName();
		else
			return "No packages";
	}
	
	public String listServices(){
		if (basket.size() != 0) {
			String serviceList = "";
			int counter = 0;
			for(Service services: basket){
				serviceList += services.getName();
				counter++;
				if (counter < basket.size())
					serviceList += ", ";
			}
			return serviceList;
		}
		else
			return "No services";
	}
	public String listCustomerServices(){
		String serviceList = "";
		int counter = 0;

		for(Service services: basket){
			if(counter%3==0)
			{
				if(counter == 0)
					serviceList = serviceList 
								+ "<div class = 'form-group'><div class='row'>";
				else
					serviceList = serviceList 
								+ "</div><div class='row'>";
			}
			serviceList = serviceList 
					+ "<div class='col-md-4 col-sm-4'>"
                	+ "<div class='panel panel-info'>"
                	+ "<div class='panel-heading'>" + services.getName() + "</div>"
                	+ "<div class='panel-body'><h4>"+ "$" + services.getCost()+ "</h4></div>"
                	+ "<div class='panel-footer'>"
                	+ 	"<div class ='radio'>"
                	+ 		"<label><input type='radio' name='serviceOptions' value='" +services.getName()+ "'/> Select </label>"
                	+ 	"</div></div>"
                	+ "</div></div>";		
			counter++;
		}
		serviceList += "</div></div>";

		return serviceList;
	}
	public List<Service> getServices(){
		List <Service> s =  new ArrayList<Service>(basket);
		return s;
	}
	public boolean addService(Service service){
		if (!basket.contains(service)) {
			basket.add(service);
			bill.addService(service);
			return true;
		}
		else
			return false;
	}
	public boolean addPackage(Package toAdd){
		if (this.myPackage != null) {
			if (this.myPackage.equals(toAdd))
				return false;
			else{
				this.removePackage(this.myPackage);
				this.addPackage(toAdd);
				return true;
			}
		}
		else {
			
			this.myPackage = toAdd;
			bill.addPackage(toAdd);
			return true;
		}
	}
	
	public boolean removeService(Service service){
		if (basket.contains(service)) {
			Date cancelDate = new Date();
			Date checkDate = new Date(service.getSubscriptionDate().getYear(), (service.getSubscriptionDate().getMonth()
					+ service.getDuration()), service.getSubscriptionDate().getDay()+ 8);
			System.out.println(dateFormat.format(checkDate));
			System.out.println(dateFormat.format(cancelDate));
			if(cancelDate.before(checkDate)){
				bill.addFee(fee);
				//add fee
			}
			basket.remove(service);
			bill.removeService(service);
			return true;
		}
		return false;	
	}
	
	public boolean removePackage(Package toRemove){
		if (this.myPackage != null){
			bill.removePackage(toRemove);
			this.myPackage = null;
			return true;
		}
		else
			return false;
	}
	
	public boolean contains(Service service){
		if(!basket.isEmpty()){
			for(Iterator<Service> iter = basket.listIterator(); iter.hasNext();){
				Service checkService = iter.next();
				if(checkService.getName() == service.getName()){
					return true;
				}
			}
		}
		return false;
	
	}
	public void setFirstName(String name){
		this.firstName = name;
	}
	public void setLastName(String name){
		this.lastName = name;
	}
	public void setPassword(String pass){
		this.password = pass;
	}
	public String getUserName(){
		return this.userName;
	}
	public String getFirstName( ){
		return this.firstName;
	}
	public String getLastName( ){
		return this.lastName;
	}
	public String getPassword(){
		return this.password;
	}
	public String getEmail(){
		return this.email;
	}
	public int getID(){
		return this.id;
	}
	public String getRule(){
		return this.accountType;
	}
	public double getBill(){
		/*double totalCost = 0;
		for( Service servicePrice : this.getServices())
		{
			totalCost += servicePrice.getCost();
		}
		return totalCost;*/
		return this.balance;
	}
	void setThreshold(double threshold){
		this.threshold = threshold;
	}
	double getThreshold(){
		return this.threshold;
	}
	@Override
	public void update(double bal) {
		this.balance += bal;
		//System.out.println(this.balance);
		if(rule.checkBalance(this)){
			
		}
		
	}

}
