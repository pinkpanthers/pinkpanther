package com.journaldev.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class addServiceServlet
 */
@WebServlet("/addServiceServlet")
public class addServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CustomerFacade cf = new CustomerFacade();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addServiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String serviceList = Representative.masterService.listServices();
		String customerList = LoginServlet.activeCustomer.listServices();
		request.setAttribute("serviceList", serviceList);
		request.setAttribute("customerList", customerList);
		request.getRequestDispatcher("addService.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Additional Services
		if(request.getParameter("AddService") != null){
			String serviceList = Representative.masterService.listServices();
			String customerList = LoginServlet.activeCustomer.listServices();
			request.setAttribute("serviceList", serviceList);
			request.setAttribute("customerList", customerList);
			request.setAttribute("customer", LoginServlet.activeCustomer);
			
			if(request.getParameter("serviceOptions") != null){
				String serviceName = request.getParameter("serviceOptions");
				Service checkService;

				if(!Representative.allServices.isEmpty()){
					for(Iterator<Service> iter = Representative.allServices.listIterator(); iter.hasNext();){
						checkService = iter.next();
						
						if(checkService.getName().equals(serviceName)){
							if(LoginServlet.activeCustomer.addService(checkService)) {
								System.out.println("ADDING SERVICE: " + serviceName);
								request.getRequestDispatcher("AccountOverview.jsp").forward(request, response);
							}
							else {
								System.out.println(serviceName + " ALREADY EXISTS");
								RequestDispatcher rd = getServletContext().getRequestDispatcher("/addService.jsp");
								PrintWriter out = response.getWriter();
								out.println("<font color=red>Service already exists.</font>");
								rd.include(request, response);
							}
						}
						/*else {
							System.out.println(serviceName + " DOESN'T EXIST");
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/addService.jsp");
							PrintWriter out = response.getWriter();
							out.println("<font color=red>Service doesn't exist.</font>");
							rd.include(request, response);
						}*/
					}
				}
				else {
					System.out.println(serviceName + " DOESN'T EXIST");
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/addService.jsp");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>Service doesn't exist.</font>");
					rd.include(request, response);
				}
			}
			else {
				System.out.println("NO CHOICE SELECTED");
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/addService.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>Please select a service from the list.</font>");
				rd.include(request, response);
			}
		}
	}
}
