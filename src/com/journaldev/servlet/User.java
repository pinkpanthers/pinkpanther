package com.journaldev.servlet;

import java.util.ArrayList;
import java.util.List;

public abstract class User {

	int id;
	String userName;
	String firstName;
	String lastName;
	String password;
	List<Service> basket = new ArrayList<Service>();
	
	public String getUserName(){
		return this.userName;
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	
	public String getLastName(){
		return this.lastName;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getPassword(){
		return this.password;
	}

}
