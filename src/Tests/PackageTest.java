package Tests;
import com.journaldev.servlet.*;
import com.journaldev.servlet.Package;

import static org.junit.Assert.*;

import org.junit.Test;

public class PackageTest {
	Package testPackage = new Package("Test",10.0,10.0, 10);
	service1 testService = new service1("TestS", 100.0, 12);
	@Test
	public void testAddPackage() {
		
		testPackage.addService(testService);
		assertTrue(Representative.allPackages.contains(testPackage));
		assertTrue(testPackage.contains(testService));
		//testPackage.removeService(testService);
		//assertTrue(testPackage.listServices().equals(""));
		
	}
	public void testRemoveService()
	{
		testPackage.removeService(testService);
		assertFalse(testPackage.contains(testService));
	}
	public void testCost(){
		assertEquals(91.0,testPackage.getCost(),0.01);
	}
	//public void
	

}
