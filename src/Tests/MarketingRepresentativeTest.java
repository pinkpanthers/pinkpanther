package Tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runners.model.InitializationError;

import com.journaldev.servlet.MarketingRepresentative;
import com.journaldev.servlet.Package;
import com.journaldev.servlet.service1;

public class MarketingRepresentativeTest {
	@Test
	public void test() throws InitializationError  {
		String user = "bill123";
		String first = "bill";
		String last = "money";
		int id = 123;
		String password = "password";
		String companyName = "copy";
		String billingName = "bank";
		int billingNum = 1;
		List<Package> packages = new ArrayList<Package>();
		service1 service = new service1();
		
		MarketingRepresentative r = new MarketingRepresentative( user, first, last, password, id);
		assertEquals(user, r.getUserName() );
		assertEquals(first, r.getFirstName());
		assertEquals(last, r.getLastName());
		assertEquals(id, r.getId());
		assertEquals( password,r.getPassword());
		
		assertEquals(null, r.createPackage());
		assertEquals(null, r.createService("Economy", 10.00));
		assertEquals(true ,r.deletePackage(packages));
		assertEquals(true, r.deleteService(service));
		
	}
}
