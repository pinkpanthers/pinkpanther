package Tests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runners.JUnit4;
import org.junit.runners.model.InitializationError;

//import com.jwt.hibernate.bean.Service;
//import com.jwt.hibernate.bean.service1;
import com.journaldev.servlet.*;


public class ServiceTest {

	@SuppressWarnings("deprecation")
	@Test
	public void test() throws InitializationError {
//		JUnit4 test = new JUnit4(null);
		String name = "stuff";
		double cost = 99.95;
		int duration = 6;
		Service ser = new service1(name, cost, duration);
		
		assertEquals(name, ser.getName());
		assertEquals(cost, ser.getCost(), 0);
		
	}

}
