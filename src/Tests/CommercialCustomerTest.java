package Tests;
import com.journaldev.servlet.CommercialCustomer;

import static org.junit.Assert.*;

import org.junit.Test;

public class CommercialCustomerTest {
	
	String user = "bill123";
	String first = "bill";
	String last = "money";
	String password = "password";
	String companyName = "copy";
	String billingName = "bank";
	String billingNumber = "777-777-7777";
	String billEmail = "cse110pinkpanthers@gmail.com";
	String email = "joanthanwmui@gmail.com";
	CommercialCustomer cc = new CommercialCustomer(companyName, user, first, last, password, email, billingName, billEmail, billingNumber);

	@Test
	public void testSettersGetters() {
		
		cc.setBillingName(billingName);
		cc.setCompanyName(companyName);
		cc.setBillingNumber(billingNumber);
		assertEquals(billingName, cc.getBillingName());
		assertEquals(billingNumber, cc.getBillingNumber());
		assertEquals(companyName, cc.getCompanyName());
		assertEquals(user, cc.getUserName());
	}
	

}
