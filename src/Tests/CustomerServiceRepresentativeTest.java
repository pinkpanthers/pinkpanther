package Tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runners.model.InitializationError;

import com.journaldev.servlet.Customer;
import com.journaldev.servlet.CustomerServiceRepresentative;
import com.journaldev.servlet.Package;
import com.journaldev.servlet.RetailCustomer;
import com.journaldev.servlet.service1;

public class CustomerServiceRepresentativeTest {
	@Test
	public void test() throws InitializationError  {
		String user = "bill123";
		String first = "bill";
		String last = "money";
		int id = 123;
		String password = "password";
		String companyName = "copy";
		String billingName = "bank";
		String email = "exam;ple@example.com";
		int billingNum = 1;
		List<Package> packages = new ArrayList<Package>();
		service1 service = new service1();
		
		Customer c = new RetailCustomer(user, first, last, password, email);
		
		CustomerServiceRepresentative r = new CustomerServiceRepresentative( user, first, last, password, id);
		assertEquals(user, r.getUserName() );
		assertEquals(first, r.getFirstName());
		assertEquals(last, r.getLastName());
		assertEquals(id, r.getId());
		assertEquals( password,r.getPassword());
		
		assertEquals(true, r.addPackage(packages, c));
		assertEquals(true, r.addService(service, c));
		assertEquals(true ,r.cancelPackage(packages, c));
		assertEquals(true, r.cancelService(service, c));
	}
}
