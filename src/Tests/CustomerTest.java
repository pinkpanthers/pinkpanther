package Tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runners.JUnit4;
import org.junit.runners.model.InitializationError;


//import com.jwt.hibernate.bean.Customer;
//import com.jwt.hibernate.bean.Service;
//import com.jwt.hibernate.bean.service1;
import com.journaldev.servlet.*;
import com.journaldev.servlet.Package;

public class CustomerTest {

	@Test
	public void testsetUserParameters() throws InitializationError {
//		JUnit4 test = new JUnit4(null);
		Customer cust = new Customer();
		String first = "Michael";
		String last = "Johnson";
		String password = "chowchow";
		
		cust.setFirstName(first);
		assertEquals(first, cust.getFirstName());
		
		cust.setLastName(last);
		assertEquals(last, cust.getLastName());	
		
		cust.setPassword(password);
		assertEquals(password, cust.getPassword());	

	}
	
	@Test
	public void testsetUserServices() throws InitializationError {
		Customer cust = new Customer();
		String name1 = "Wireless";
		String name2 = "Phone Plan";
		double cost1 = 99.99;
		double cost2 = 30.11;
		int duration1 = 12;
		int duration2 = 13;
		Service ser1 = new service1(name1, cost1, duration1);
		Service ser2 = new service1(name2, cost2, duration2);
		
		cust.addService(ser1);
		List<Service> list = cust.getServices();
		assertEquals(true, list.contains(ser1));
		assertEquals(1,list.size());
		
		cust.addService(ser2);
		list = cust.getServices();
		assertEquals(true, list.contains(ser1));
		assertEquals(true, list.contains(ser2));
		assertEquals(2,list.size());
		
		cust.removeService(ser1);
		assertEquals(false, cust.getServices().contains(ser1));
		assertEquals(1,cust.getServices().size());		
	}
	
	@Test
	public void testlistPackages() {
		Customer cust = new Customer();
		Package pkg = new Package ("aPackage", 0, 0, 10);
		
		assertEquals("Checking empty functionality", cust.listPackages(), "No packages");
		cust.addPackage(pkg);
		
		assertEquals("Checking proper addition of packages", cust.listPackages(), "aPackage");
		
	}
	
	@Test
	public void testlistServices() {
		Customer cust = new Customer();
		Service service = new service1 ("aService", 10, 10);
		
		assertEquals("Checking empty functionality", cust.listServices(), "No services");
		cust.addService(service);
		
		assertEquals("Checking proper addition of packages", cust.listServices(), "aService");
		
	}

}
