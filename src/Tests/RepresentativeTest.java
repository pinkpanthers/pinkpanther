package Tests;
import com.journaldev.servlet.Representative;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runners.model.InitializationError;

public class RepresentativeTest {

	@Test
	public void test() throws InitializationError  {
		String user = "bill123";
		String first = "bill";
		String last = "money";
		int id = 123;
		String password = "password";
		String companyName = "copy";
		String billingName = "bank";
		int billingNum = 1;
		Representative r = new Representative( user, first, last, password, id);
		assertEquals(user, r.getUserName() );
		assertEquals(first, r.getFirstName());
		assertEquals(last, r.getLastName());
		assertEquals(id, r.getId());
		assertEquals( password,r.getPassword());
		
	}
	

}
