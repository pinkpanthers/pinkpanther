package Tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ CustomerTest.class,
	ServiceTest.class, 
	RepresentativeTest.class, 
	RetailCustomerTest.class, 
	PackageTest.class,
	CommercialCustomerTest.class,
	MarketingRepresentativeTest.class,
	CustomerServiceRepresentativeTest.class,
	BillTests.class,
	CustomerTest.class})
public class AllTests {

}
