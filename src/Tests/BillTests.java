package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.journaldev.servlet.*;

public class BillTests {
	
	Bill bill = new Bill();
	Customer c = new RetailCustomer("user", "first", "last", "password", "email");
	Service s = new service1("name", 9.99, 12);
	@SuppressWarnings("deprecation")
	@Test
	public void testObserverPattern() {
		bill.registerObserver(c);
		bill.addFee(11.0);
		assertEquals(11.0,c.getBill(),0.01);
		bill.removeObserver(c);
		bill.addFee(13.0);
		assertEquals(11.0,c.getBill(),0.01);
		bill.registerObserver(c);
		bill.addService(s);
		assertEquals(20.99, c.getBill(), 0.01);
	}
	

}
