package Tests;

import static org.junit.Assert.*;


import org.junit.Test;
import org.junit.runners.model.InitializationError;

import com.journaldev.servlet.RetailCustomer;

public class RetailCustomerTest {

	@Test
	public void test() throws InitializationError {
		String user = "bill123";
		String first = "bill";
		String last = "money";
		String password = "password";

		String email = "example@example.com";
		RetailCustomer c1 = new RetailCustomer( user, first, last, password, email);
		assertEquals(user, c1.getUserName() );
		assertEquals(first, c1.getFirstName());
		assertEquals(last, c1.getLastName());
		assertEquals( password,c1.getPassword());

	}

}
