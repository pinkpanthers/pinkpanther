<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>View My Bill</title>
<!-- BOOTSTRAP STYLES-->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">TeleCom</a>
			</div>

			<div
				style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">
				<a href="AccountLogin.html" class="btn btn-danger square-btn-adjust">Logout</a>
			</div>
		</nav>
		<!-- /. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">
					<li class="text-center"><img src="assets/img/pinkPanther.png"
						class="user-image img-responsive" /></li>


					<li><a  href="http://localhost:8080/LoginExample/LoginServlet"><i
							class="fa fa-home fa-3x"></i>My Account Overview</a></li>

					<li><a href="ServiceManagement"><i
							class="fa fa-sitemap fa-3x"></i>Manage my services<span
							class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="http://localhost:8080/LoginExample/addServiceServlet">Add Service</a></li>
							<li><a href="http://localhost:8080/LoginExample/CancelServiceServlet">Cancel Service</a></li>
							<li><a href="http://localhost:8080/LoginExample/addPackageServlet">Add Package</a></li>
							<li><a href="http://localhost:8080/LoginExample/CancelPackageServlet">Cancel Package</a></li>
						</ul></li>

					<li><a class="active-menu" href="#"><i
							class="fa fa-desktop fa-3x"></i>View My Bill</a></li>
				</ul>

			</div>

		</nav>
		<!-- /. NAV SIDE  -->
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h2>Bill Summary</h2>
					</div>
				</div>
				<!-- /. ROW  -->
				<hr/>
				<div>
					<h5>Current Threshold: ${placeholder}${threshold}</h5>
					<form action = "BillServlet" method = "post">
						<h5>Set Threshold amount before being notified: $<input type = "text" value = "0.00" name = "Threshold"> <input type="submit" value="Set"></h5>
						</hr>
					</form>
					<h5></h5>
				</div>
					<div class="panel panel-default">
                        <div class="panel-heading">
                            Subscribed Services & Packages
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    	<pre><h5>${subscribedServices}</h5></pre>
                                    	<pre><h5>${subscribedPackages}</h5></pre>
                                    	
                                    	<h5><b>TOTAL:</b> $${totalCost}
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
				<hr/>

			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->
	<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
	<!-- JQUERY SCRIPTS -->
	<script src="assets/js/jquery-1.10.2.js"></script>
	<!-- BOOTSTRAP SCRIPTS -->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- METISMENU SCRIPTS -->
	<script src="assets/js/jquery.metisMenu.js"></script>
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js/custom.js"></script>


</body>
</html>
