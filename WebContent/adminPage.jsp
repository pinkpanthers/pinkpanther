<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Marketing Admin</title>
<!-- BOOTSTRAP STYLES-->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">TeleCom</a>
			</div>

			<div
				style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">
				<a href="AccountLogin.html" class="btn btn-danger square-btn-adjust">Logout</a>
			</div>
		</nav>
		<!-- /. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">
					<li class="text-center"><img src="assets/img/pinkPanther.png"
						class="user-image img-responsive" /></li>


					<li><a class="active-menu" href="#"><i
							class="fa fa-home fa-3x"></i>Marketing Representative Home Page</a></li>

				</ul>

			</div>

		</nav>
		<!-- /. NAV SIDE  -->
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h3>Create a new Service</h3>
						<h3><pre>${serviceList}</pre></h3>
						<form action="CreateServiceServlet" method="post">
						
							Service Name: <input type="text" name="serviceName"><br>
							Service Price: <input type="text" name="price"><br>
							Duration (Months): <input type="text" name="duration"><br>
							<input type="submit" value ="Create" name="createService">
			
						<p>
			
						</p>
						</form>
						
						<h3>Delete a Service</h3>
						<h5>${errorDeletingService}</h5>
						<h3><pre>${serviceList}</pre></h3>
						<form action="DeleteServiceServlet" method="post">
						
							Service Name: <input type="text" name="serviceName"><br>
							<input type="submit" value ="Delete" name="deleteService">
			
						<p>
						
						</p>
						</form>
						
						<h3>Create a new Package</h3>
						<h5>${errorMessage}</h5>
						<h3><pre>${packageList}</pre></h3>
						<form action="CreatePackageServlet" method="post">
						
							Package Name:  <input type="text" name="packageName"><br>
							Service 1:	   <input type="text" name="service1"><br>
							Service 2:	   <input type="text" name="service2"><br>
							Service 3:	   <input type="text" name="service3"><br>
							Discount Fixed:		<input type="text" name="Fixed"><br>
							Discount Percent (0-99):	<input type="text" name="Percent"><br>
							Duration (Months):	<input type="text" name="duration"><br>
							<input type="submit" value ="Create" name="createPackage">
								
						<p>
								
						</p>
						</form>
						
						<h3>Add to an existing Package</h3>
						<h5>${errorChangingPackage}</h5>
						<form action="CreatePackageServlet" method="post">
							
							Package to Change:  <input type="text" name="packageChange"><br>
							Service To Add:	   <input type="text" name="serviceAdd"><br>
					
							<input type="submit" value ="Modify" name="modifyPackage">
								
						<p>
								
						</p>
						</form>
						
						<h3>Delete a Package</h3>
						<h5>${errorDeletingPackage}</h5>
						<h3><pre>${packageList}</pre></h3>
						<form action="DeletePackageServlet" method="post">
						
							Package Name:  <input type="text" name="packageName"><br>
							<input type="submit" value ="Delete" name="deletePackage">
								
						<p>
								
						</p>	
						</form>

					</div>
				</div>
				<!-- /. ROW  -->
				<hr />

			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->
	<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
	<!-- JQUERY SCRIPTS -->
	<script src="assets/js/jquery-1.10.2.js"></script>
	<!-- BOOTSTRAP SCRIPTS -->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- METISMENU SCRIPTS -->
	<script src="assets/js/jquery.metisMenu.js"></script>
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js/custom.js"></script>


</body>
</html>
