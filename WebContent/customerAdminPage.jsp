<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Customer Service Admin</title>
<!-- BOOTSTRAP STYLES-->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">TeleCom</a>
			</div>

			<div
				style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">
				<a href="AccountLogin.html" class="btn btn-danger square-btn-adjust">Logout</a>
			</div>
		</nav>
		<!-- /. NAV TOP  -->
		<nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav" id="main-menu">
					<li class="text-center"><img src="assets/img/pinkPanther.png"
						class="user-image img-responsive" /></li>


					<li><a class="active-menu" href="#"><i
							class="fa fa-home fa-3x"></i>Customer Service Representative Home Page</a></li>

				</ul>

			</div>

		</nav>
		<!-- /. NAV SIDE  -->
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row">
					<div class="col-md-12">
						<h3>Modify Customer's Account</h3>
						<h3><pre>${customerList}</pre></h3>
						<form action="AdminServlet" method="post">

							Username: <input type="text" name="user"> <br> 
							<input type="submit" value="Modify" name="adminModify">
						<p>
			
						</p>
						</form>

					</div>
				</div>
				<!-- /. ROW  -->
				<hr />
				<div class="container-fluid">
									    <section class="container">
											<div class="container-page">	
											<form action="CustomerRepServlet" method="post">			
												<div class="col-md-6">
														<div class="form-group col-lg-12">
															<label>Company Name</label>
															<input type="text" name="companyName" class="form-control" placeholder="Company Name">
														</div>
														
														<div class="form-group col-lg-5">
															<label>First Name</label>
															<input type="text" name="companyfirstname" class="form-control" placeholder="First Name">
														</div>
														
														
														<div class="form-group col-lg-5">
															<label>Last Name</label>
															<input type="text" name="companylastname" class="form-control" placeholder="Last Name">
														</div>
														
														<div class="form-group col-lg-6">
															<label>Email Address</label>
															<input type="text" name="companyemail" class="form-control" placeholder="Email">
														</div>
														
														<div class="form-group col-lg-6">
															<label>Phone Number</label>
															<input type="text" name="companyphone" class="form-control" placeholder="Phone">
														</div>
														
														<div class="form-group col-lg-12">
															<label>Username</label>
															<input type="text" name="companyuser" class="form-control" placeholder=Username>
														</div>
																
														<div class="form-group col-lg-12">
															<label>Password</label>
															<input type="password" name="companypwd" class="form-control" placeholder=Password>
														</div>
														
														<div class="form-group col-lg-12">
															<label>Billing Department Name</label>
															<input type="text" name="billName" class="form-control" placeholder="Billing Department Name">
														</div>	
														
														<div class="form-group col-lg-12">
															<label>Billing Department Email</label>
															<input type="text" name="billEmail" class="form-control" placeholder="Billing Department Email">
														</div>
														
														<div class="form-group col-lg-12">
															<label>Billing Department Number</label>
															<input type="text" name="billNumber" class="form-control" placeholder="Billing Department Number">
														</div>			

														<div class="row" >
															<div class="col-md-12">
															<h3 class="dark-grey"><br>Terms and Conditions</h3>
																By clicking on "Register" you agree to TeleCom's Terms and Conditions
																<p>
																While rare, prices are subject to change based on exchange rate fluctuations - 
																should such a fluctuation happen, we may request an additional payment. You have the option to request a full refund or to pay the new price. (Paragraph 13.5.8)
																Should there be an error in the description or pricing of a product, we will provide you with a full refund (Paragraph 13.5.6)
																Acceptance of an order by us is dependent on our suppliers ability to provide the product. (Paragraph 13.5.6)
																</p>
															</div>
														</div>
														<button type="submit" class="btn btn-primary" name="registerAdmin" value="commercial">Register Commercial</button>  By clicking you agree to the Terms and Conditions	
													</div>
												</form>
											</div>
										</section>
									</div>
			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->
	<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
	<!-- JQUERY SCRIPTS -->
	<script src="assets/js/jquery-1.10.2.js"></script>
	<!-- BOOTSTRAP SCRIPTS -->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- METISMENU SCRIPTS -->
	<script src="assets/js/jquery.metisMenu.js"></script>
	<!-- CUSTOM SCRIPTS -->
	<script src="assets/js/custom.js"></script>


</body>
</html>
